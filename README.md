# ciscobackup-posh

Backup script written in powershell for **Cisco** devices(IOS and Nexus). The script may be erratic on other platforms.

To run the script successfully, 

- create a directory named **backupscript** at the root of a named drive(ex: C: or D:). 

- create the following files: **hosts.txt**, **commands.txt** and a **hosts_IP.CSV** respectively inside the backupscript directory.

- Edit the hosts_IP.CSV file and write IP in the first cell(A1) and HOSTNAME in next(B1) and then save the file.

- Put the IP of the devices to be backed up in the *hosts.txt* file and the command( *show run* ) in the commands.txt file.



First run of the script will take time in order build the hosts file and for ssh kex.



