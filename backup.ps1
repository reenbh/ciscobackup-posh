Write-Host "Welcome to automated backup script.`nPlease enter the credentials and rest of the action will follow.`nIf you are running this script at night,`nplease make sure you start it before the day is over.`n`n" -ForegroundColor Yellow
#Write-Host "`nPlease inform Reed if new devices are to be added.`n`n" -ForegroundColor Green

$uname = Read-Host -Prompt 'username'
$secpass= Read-Host 'password' -AsSecureString


$warning_text = "********Backed up through automated script. Please do not modify********`n`n"

$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secpass)
$pass = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)

$date = Get-Date -format dd-MM-yyyy
$monthYear = Get-Date -Format MMM-yyyy

#partition-info
$part = ((Get-Location).Path.Split("\")).Get(0)

#Fetch the pre-stored key pairs.
#$table = Import-Csv -Path $part\\backupscript\hosts_IP.csv -Header "column1","column2"
$table = Import-Csv -Path $part\\backupscript\hosts_IP.csv
$hashtable = @{}

#Build the in memory cache
foreach($row in $table)
{
    $hashtable[$row.IP] = $row.HOSTNAME
    
}


#DIRECTORY CREATION/VALIDATION

#Backup dir
$test = Test-Path $part\\Backups -PathType Container
If( $test -eq $FALSE){
New-Item $part\\Backups -ItemType directory}

#Cretes a dated directory where all backups will be stored
$pathvalue = Test-Path $part\\Backups\$date
If($pathvalue -eq $TRUE)
{$time = "_" + (Get-Date -format HH:mm)
$date = $date + ($time -replace '[:]')} 

New-Item $part\\Backups\$date -type directory | Out-Null
Write-Host "`nCreated directory: $part\\Backups\$date"

#Read commands
$commands = Get-Content $part\\backupscript\commands.txt

#Checking availability of the commands file
$validate = Test-Path $part\\backupscript\commands.txt
If($validate -eq $FALSE){echo "Attention! Commands not available.`nPlease make sure command file is in same directory as the script" -foregroundcolor "Red";Return}




#hosts to be backed up.

$hosts = Get-Content $part\\backupscript\hosts.txt


<#Pulls the backup from the individual devices. 
Names individual files according to the hostname of the device.
Prints backup confirmation.#>

start-sleep 1
Write-Host "`n`r"
Write-Host "Starting backup..."
Write-Host "`n`r"
start-sleep 2

 
Foreach($myhost in $hosts){
$runvalue = "y" | .\plink.exe -ssh -2 -l $uname -pw $pass $myhost $commands
#echo $runvalue
$gettime = "Backup time: " + (Get-Date).ToString() + "`n`n`r`r"
Add-Content $part\\Backups\$date\tmp.txt $gettime
Add-Content $part\\Backups\$date\tmp.txt $runvalue


if($hashtable.ContainsKey($myhost) -eq $FALSE) {
$temp = Get-Content $part\\Backups\$date\tmp.txt | findstr hostname 
$name = $temp.Remove(0,8)
Rename-Item -Path "$part\\Backups\$date\tmp.txt" -NewName "$name.txt"
$hashed = @{ "IP"= $myhost 
             "HOSTNAME" = $name 
           }
$new_row = New-Object PsObject -Property $hashed
Export-Csv -Path $part\\backupscript\hosts_IP.csv -InputObject $new_row -Append -Force 
}
Else {
$name = $hashtable.$myhost
Rename-Item -Path "$part\Backups\$date\tmp.txt" -NewName "$name.txt"
}
<#If (((Test-Path $part\\Backups\$date\$name.txt -PathType leaf) -eq $FALSE) -And ((Get-Item $part\\Backups\$date\$name.txt).length -lt 2kb)){Write-Host "[FAILED] : $name" -ForegroundColor DarkRed}
Else { Write-Host "[SUCCESS] : $name" -ForegroundColor DarkGreen}
start-sleep 5#>
If (((Get-Item $part\\Backups\$date\$name.txt).length -lt 2kb) -or (Get-Content $part\\Backups\$date\$name.txt | findstr hostname) -eq $FALSE){Write-Host "[FAILED]  : $name" -ForegroundColor DarkRed ; Add-Content $part\\backupscript\Logs\$date.txt -value ("$name : FAILED, run at " + (Get-Date).ToString())}
Else { Write-Host "[SUCCESS] : $name" -ForegroundColor DarkGreen ; Add-Content $part\\backupscript\Logs\$date.txt -value ("$name : SUCCESS, run at " + (Get-Date).ToString())}
start-sleep 5
}
Add-Content $part\\backupscript\Logs\$date.txt -value ("`n=============================================================`n")

#$temp_device_list | ForEach-Object{ [pscustomobject]$_ } | Export-CSV -Path $part\\backupscript\hosts_IP.csv

#Write-Host "`n`n`[ATTENTION!] Due to some limitation, SZ2 core switches have been excluded`n from this script's execution and backup has to be taken manually.`n" -ForegroundColor Yellow


start-sleep 2
Write-Host "`n`nCopying to Central storage..." -ForegroundColor Gray
#copy to SMB storage
#$sharePath = '\\10.10.1.101\ITSS_Group_Share\ITSS_Infobahn\Non_Sensitive\Reed'
$sharePath = '\\10.10.1.251\ITSS_Group_Share\ITSS_Infobahn\Sensitive_Seclore_Protected\NOC\Backup'
$path_isvalid = Test-Path $sharePath\$monthYear\$date

if ($path_isvalid -eq $false) {
New-Item $sharePath\$monthYear\$date -type Directory | Out-Null
}

Get-ChildItem $part\\Backups\$date | Copy-Item -Destination $sharePath\$monthYear\$date\ -Recurse

start-sleep 2
Write-Host "`nDevice backup files copied to $sharePath\$monthYear\$date\!" -ForegroundColor DarkGreen

Write-Host "`n`nScript execution completed." -ForegroundColor Gray
Read-Host -Prompt "`nPlease press any key to exit..."



#Authored and maintained by R.N. Bhatta, 2017-2022
